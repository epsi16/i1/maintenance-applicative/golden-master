package com.gildedrose;

import com.google.gson.Gson;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TexttestFixture {

    public static void main(String[] args) throws IOException, ParseException {
        launchGame(true);
    }

    /**
     *
     * @param savetofile (save data to test.json file if savetofile == true)
     * @return itemList in json
     *
     */
    public static String launchGame(boolean savetofile) {
        System.out.println("OMGHAI!");
        Constants constants = new Constants();

        Item[] items = new Item[]{
                new Item(constants.dexterity, 10, 20), //
                new Item(constants.aged, 2, 0), //
                new Item(constants.elixir, 5, 7), //
                new Item(constants.sulfuras, 0, 80), //
                new Item(constants.sulfuras, -1, 80),
                new Item(constants.backstage, 15, 20),
                new Item(constants.backstage, 10, 49),
                new Item(constants.backstage, 5, 49),
                // this conjured item does not work properly yet
                new Item(constants.conjured, 3, 6)};

        GildedRose app = new GildedRose(items);

        int days = 2;

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + i + " --------");
            System.out.println("name, sellIn, quality");
            for (Item item : items) {
                System.out.println(item);
            }
            app.updateQuality();
        }
        String json = new Gson().toJson(items);
        if (savetofile) {
            saveToFile(json);
        }
        return json;
    }


    /**
     * Method wich save data to test.json file
     * @param items
     */
    public static void saveToFile(String items) {
        File myObj = new File("src/test/java/com/gildedrose/assets/test.json");
        try {
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter myWriter;
            myWriter = new FileWriter("src/test/java/com/gildedrose/assets/test.json");
            myWriter.write(items);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
