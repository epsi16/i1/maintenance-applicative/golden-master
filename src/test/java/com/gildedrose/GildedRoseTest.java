package com.gildedrose;

import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    TexttestFixture texttestFixture = new TexttestFixture();

    @Test
    void foo() {
        Item[] items = new Item[] { new Item("foo", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
    }

    @Test
    void testMatch() {
        String newItems = null;
        newItems = TexttestFixture.launchGame(false);
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("src/test/java/com/gildedrose/assets/test.json")) {
            Object obj = jsonParser.parse(reader);
            JSONArray savedItems = (JSONArray) obj;
            String savedItemsString = savedItems.toJSONString();
            assertEquals(newItems, savedItemsString);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

    }

}
