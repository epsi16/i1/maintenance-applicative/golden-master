package com.gildedrose;

class GildedRose {
    Item[] items;
    Constants constants = new Constants();


    public GildedRose(Item[] items) {
        this.items = items;
    }


    public void updateQuality() {
        for (Item item : items) {

            if (!item.name.equals(constants.aged) && !item.name.equals(constants.backstage) && (!item.name.equals(constants.sulfuras))) {
                item.quality += - 1 ;
            } else if (item.quality < 50) {
                item.quality += 1;
            }

            item.sellIn = (!item.name.equals(constants.sulfuras)) ? item.sellIn += -1 : item.sellIn;

            if (item.sellIn < 0) {
                if (!item.name.equals(constants.aged)) {
                    item.quality = (!item.name.equals(constants.backstage)) ? (((item.quality > 0) && (!item.name.equals(constants.sulfuras))) ? item.quality += -1 : item.quality) : 0;
                } else {
                    item.quality = item.quality < 50 ? item.quality + 1 : item.quality;
                }
            }
        }
    }
}
