package com.gildedrose;

public class Constants {

    public String backstage = "Backstage passes to a TAFKAL80ETC concert";
    public String aged = "Aged Brie";
    public String sulfuras = "Sulfuras, Hand of Ragnaros";
    public String dexterity = "+5 Dexterity Vest";
    public String elixir = "Elixir of the Mongoose";
    public String conjured = "Conjured Mana Cake";

}
